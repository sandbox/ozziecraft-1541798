<?php

/**
 * @file
 * Theme setting callbacks for the Minecraft theme.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function minecraft_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['minecraft_subheading'] = array(
    '#type' => 'textfield',
    '#title' => t('Sub-heading'),
    '#default_value' => theme_get_setting('minecraft_subheading'),
    '#description' => t('Displays a sub-heading below the main title. Different from the "slogan" because it will display on every page.'),
    '#weight' => -2,
  );
  
}